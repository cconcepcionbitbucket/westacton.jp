# World Map - Westacton

## What is World Map - Westacton?

Display World Map. Focus on current location. Adding PIN by clicking map or textbox. Zoom in and out. Share with other users.

This repository holds the distributable version of the project. It has been built from the
[development repository](https://bitbucket.org/cconcepcionbitbucket/westacton.jp/src/master/).

## Installation

1. Import data from westacton_jp.sql into your database
2. Run index.php on browser
