<?php

/**
 * PHP Developer - Westacton
 * World Map - Display World Map. Focus on current location. Adding PIN by clicking map or textbox. Zoom in and out. Share with other users.
 */

class Map
{
    private $mysqli;

    function __construct() {
        $this->initialize();
    }

    function __destruct() {
        $this->dbClose();
    }

    function initialize() {
        $this->dbConnect();        
    }

    function dbConnect() {
        $this->mysqli = new mysqli("localhost", "root", "", "westacton.jp");

        // Check connection
        if ($this->mysqli->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->mysqli->connect_error;
            exit();
        }      
    }

    function dbQuery($sqlCmd) {
        $results = [];

        // Perform query
        if ($result = $this->mysqli->query($sqlCmd)) {
            $results = $result->fetch_all(MYSQLI_ASSOC);
            
            // Free result set
            $result->free_result();            
        }  

        return $results;
    }

    function dbInsert($sqlCmd) {
        $results = [];

        // Perform query
        if ($result = $this->mysqli->query($sqlCmd)) {
        }  

        return $results;
    }

    function dbClose() {
        $this->mysqli->close();
    }

    function exportJSON($data = []) {
        header('Content-type: application/json');
        return json_encode($data);
    }

    function loadMap() {
        $results = $this->dbQuery("
                    SELECT * FROM map 
                    ");

        $map = $this->getGeocode($this->getAddress());
        
        $results = ["results"   =>  $results,
                    "latitude"  =>  $map['latitude'],
                    "longitude"  =>  $map['longitude']];
        
        echo $this->exportJSON($results);
    }

    function createMap($data) {
        $name = $data->lat . ', ' . $data->lng;
        $results = $this->dbInsert("
                    INSERT INTO map (name, address, latitude, longitude) VALUES ('{$name}', '{$name}', '{$data->lat}', '{$data->lng}')
                    ");

        $results = ["status"   =>  $data];
        
        echo $this->exportJSON($results);
    }

    function getAddress() {
    
        // get user address base on public ip address
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if ($ip == '127.0.0.1')
            $ip = file_get_contents('http://checkip.dyndns.com/');
        $pattern = "/<body ?.*>(.*)<\/body>/";
        preg_match($pattern, $ip, $matches);
        $match = explode(':', $matches[1]);
        $ip = trim($match[1]);
        $json = file_get_contents('http://ip-get-geolocation.com/api/json/' . $ip);
        $obj = json_decode($json);
        if ($obj->status !== 'fail') {
            $address = urlencode($obj->city . ', ' . $obj->country);
        } else {            
            $json = file_get_contents('http://ip-api.com/json/' . $ip);
            $obj = json_decode($json);
            if ($obj->status == 'success') 
                $address = urlencode($obj->city . ', ' . $obj->country);
            else
                $address = 'New York City, United States';
        }
    
        return $address;
    }
    
    function getGeocode($address) {
     
        // url encode the address
        $address = urlencode($address);
        
        // google map geocode api url
        $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key=AIzaSyBy8eV540vGd5AF2C_aOwBa2UFz_WjOVpA";
     
        // get the json response from url
        $resp_json = file_get_contents($url);
        
        // decode the json response
        $resp = json_decode($resp_json, true);
        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){
            //define empty array
             $data_arr = array(); 
            // get the important data
            $data_arr['latitude'] = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : '';
            $data_arr['longitude'] = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : '';
            $data_arr['name'] = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : '';
            
            // verify if data is exist
            if(!empty($data_arr) && !empty($data_arr['latitude']) && !empty($data_arr['longitude'])){
    
                return $data_arr;
                
            }else{
                return false;
            }
            
        }else{
            return false;
        }
    }
}

$map = new Map();
if (isset($_GET["action"]))
{
    if ($_GET["action"] == "loadMap")
        $map->loadMap();
    elseif ($_GET["action"] == "createMap")
        $map->createMap(json_decode($_POST["data"]));
} else {
?>
<!DOCTYPE html>
<html>

    <head>
        <title>World Map - Westacton</title>
        <meta name="viewport" content="initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.js">
        </script>
        <meta charset="utf-8">
        <style>
        #map {
            height: 100%;
        }

        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        </style>
    </head>

    <body>
        <div id="form">
            latitude: <input type="text" name="lat" id="lat" value="">
            longitude: <input type="text" name="lng" id="lng" value="">
            <input type="submit" name="submit" id="submit" value="Add a pin on the map!">
        </div>
        <div id="map"></div>

        <script type="text/javascript">
        var map;
        var marker;

        function getData() {
            $.ajax({
                url: "index.php?action=loadMap",
                async: true,
                dataType: 'json',
                success: function(result) {
                    console.log(result);
                    //load map
                    loadMap(result);
                },
                error: function(x, e) {
                    console.log(e);
                }
            });
        }

        function saveData(data) {
            $.ajax({
                url: "index.php?action=createMap",
                async: true,
                type: "post",
                data: {
                    data: data
                },
                success: function(result) {
                    console.log(result);

                },
                error: function(x, e) {
                    console.log(e);
                }
            });
        }

        function loadMap(data) {
            var map_options = {
                zoom: 9,
                center: new google.maps.LatLng(data['latitude'], data['longitude'])
            }

            map = new google.maps.Map(document.getElementById("map"), map_options);

            // Create the initial InfoWindow.
            var infoWindowMain = new google.maps.InfoWindow({
                content: "Click the map to add a pin!",
                position: {
                    lat: data['latitude'],
                    lng: data['longitude']
                },
            });

            var infowindow = new google.maps.InfoWindow(),
                lat, lng;

            infoWindowMain.open(map);
            // Configure the click listener.
            map.addListener("click", (mapsMouseEvent) => {
                // Close the current InfoWindow.
                infoWindowMain.close();
                // Create a new InfoWindow.
                infoWindowMain = new google.maps.InfoWindow({
                    position: mapsMouseEvent.latLng,
                });
                infoWindowMain.setContent(
                    JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
                );
                //infoWindowMain.open(map);

                lat = mapsMouseEvent.latLng.lat();
                lng = mapsMouseEvent.latLng.lng();
                name = lat + ', ' + lng;

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    name: name,
                    map: map
                });
                google.maps.event.addListener(marker, 'click', function(e) {
                    infowindow.setContent(this.name);
                    infowindow.open(map, this);
                }.bind(marker));

                saveData(JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2));
            });

            var json = data['results'];
            for (var o in json) {

                lat = json[o].latitude;
                lng = json[o].longitude;
                name = json[o].name;

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    name: name,
                    map: map
                });
                google.maps.event.addListener(marker, 'click', function(e) {
                    infowindow.setContent(this.name);
                    infowindow.open(map, this);
                }.bind(marker));
            }
        }

        $("#submit").click(function() {
            var lat = $("#lat").val();
            var lng = $("#lng").val();
            var name = lat + ', ' + lng;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                name: name,
                map: map
            });
            google.maps.event.addListener(marker, 'click', function(e) {
                infowindow.setContent(this.name);
                infowindow.open(map, this);
            }.bind(marker));

            saveData('{' + '"lat":' + $("#lat").val() + ',' + '"lng":' + $("#lng").val() + '}');
        });
        </script>

        <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBy8eV540vGd5AF2C_aOwBa2UFz_WjOVpA&callback=getData"
            async defer>
        </script>

    </body>

</html>
<?php
}
?>